<div class="row no-pad bg-azul ">
	<div class="col-sm-4 mt-150 pad-3">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/refresco-final.png' ?>" class="center-block img-responsive">
	</div>

	<div class="col-sm-8 mt-150 pad-3">
		<p class="tx-blanco tx-40">Sin importar cuál consumas, los mitos que envolvían a los orígenes de los sustitutos del azúcar han quedado atrás y ahora su uso te puede ayudar a <strong>cumplir tus objetivos alimenticios.</strong></p>
	</div>
</div>