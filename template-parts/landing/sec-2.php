<div class="row no-pad">
	<div class="col-sm-6 bg-gris pad-1">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/bascula.png' ?>" class="center-block img-responsive">
	</div>

	<div class="col-sm-6 bg-azul pad-1">
		<h2 class="tx-blanco tx-40">Aliados en el control de peso</h2>
		<p class="tx-blanco tx-20 tx-thin">Pueden jugar un rol importante en programas de manejo de peso que incluyan, además opciones nutricionales, actividad física.</p>
	</div>
</div>


<div class="row no-pad">
	<div class="col-sm-6 bg-gris pad-1">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/vigilancia.png' ?>" class="center-block">
	</div>

	<div class="col-sm-6 bg-amarillo pad-1">
		<h2 class="tx-blanco tx-40">Bajo estricta vigilancia</h2>
		<p class="tx-negro tx-20 tx-thin">La Administración de Alimentos y Medicamentos (FDA) en los Estados Unidos, revisa estos productos antes de aprobarlos para consumo humano.</p>
	</div>
</div>


<div class="row no-pad">
	<div class="col-sm-6 bg-gris pad-1">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/mexico.png' ?>" class="center-block">
	</div>

	<div class="col-sm-6 bg-verde pad-1">
		<h2 class="tx-blanco tx-40">Disponibles en México</h2>
		<p class="tx-negro tx-20 tx-thin">Los seis sustitutos comercializados en México son: sucralosa, sacarina, aspartame, Ace-K, neotame y estevia.</p>
	</div>
</div>