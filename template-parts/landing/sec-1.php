<div class="row no-pad">
	<div class="col-sm-10 col-sm-offset-1">
		<h1 class="tx-azul text-center copy-1 text-uppercase">Sustitutos de azúcar,
			<br><span class="tx-verde">dulces y seguros</span></h1>
	</div>
</div>

<div class="row no-pad">
	<div class="col-sm-10 col-sm-offset-1">
		<p class="text-center tx-gris copy-2">Los sustitutos de azúcar endulzan los alimentos y bebidas sin añadir calorías, y sin las graves consecuencias a la salud asociadas al azúcar: diabetes, hígado graso, insuficiencia renal, obesidad y deterioro dental, entre otros.</p>
	</div>
</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<p class="text-center tx-verde copy-2"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></p>
	</div>
</div>