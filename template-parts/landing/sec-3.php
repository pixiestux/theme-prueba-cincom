<div class="row no-pad">
	<div class="col-xs-12">
		<h2 class="text-center text-uppercase titulo-interesante">Datos interesantes</h2>
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/1-estevia.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24">Se obtienen por procesos sintéticos, <span class="tx-azul">aunque provienen de sustancias naturales</span></p>
	</div>
</div>



<div class="row no-pad row-pad-2">

	<div class="col-sm-4 pad-2 visible-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/2-sacarina.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24 text-right">Los más consumidos son la <span class="tx-verde">la sacarina y el aspartame</span></p>
	</div>

	<div class="col-sm-4 pad-2 hidden-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/2-sacarina.png' ?>" class="center-block img-responsive img-circle">
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/3-galleta.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24">La sacarina es el endulzante más antiguo, y es de <span class="tx-azul">300 a 500 veces más dulce que el azúcar.</span></p>
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2 visible-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/4-bebida.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24 text-right">Como mezcla de dos aminoácidos naturales, ácido aspártico y la fenilalanina, <span class="tx-verde">se obtiene el aspartame.</span></p>
	</div>

	<div class="col-sm-4 pad-2 hidden-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/4-bebida.png' ?>" class="center-block img-responsive img-circle">
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/5-pan.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24">El acesulfame k es ideal para preparar productos de panificación por su <span class="tx-azul">estabilidad en temperaturas altas.</span></p>
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2 visible-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/6-fruta.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24 text-right">Los sustitutos naturales son  <span class="tx-verde">la fructosa y el estevia.</span></p>
	</div>

	<div class="col-sm-4 pad-2 hidden-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/6-fruta.png' ?>" class="center-block img-responsive img-circle">
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/7-miel.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24">Fructosa, aporta 4 calorías por gramo y <span class="tx-azul">se encuentra en las frutas y la miel.</span></p>
	</div>
</div>



<div class="row no-pad row-pad-2">
	<div class="col-sm-4 pad-2 visible-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/8-estevia2.png' ?>" class="center-block img-responsive img-circle">
	</div>

	<div class="col-sm-8 pad-2">
		<p class="tx-gris tx-24 text-right">Estevia, un sustituto descubierto en Sudamérica con un poder endulzante <span class="tx-verde">300 veces mayor al del azúcar.</span></p>
	</div>

	<div class="col-sm-4 pad-2 hidden-xs">
		<img src="<?php echo get_template_directory_uri().'/template-parts/landing/img/8-estevia2.png' ?>" class="center-block img-responsive img-circle">
	</div>
</div>