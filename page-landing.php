<?php
/**
 * Template Name: Prueba Landing
 *
 * @package   prueba_grupocincom
 */

get_header();

?>

<?php get_template_part( 'template-parts/landing/top' ); ?>


<div class="container-fluid ">

	<?php get_template_part( 'template-parts/landing/sec-1' ); ?>

	<?php get_template_part( 'template-parts/landing/sec-2' ); ?>

	<?php get_template_part( 'template-parts/landing/sec-3' ); ?>

	<?php get_template_part( 'template-parts/landing/sec-4' ); ?>

</div> <!-- Cierre de contenedor principal -->

<?php get_template_part( 'template-parts/landing/bottom' ); ?>


<?php
/*----------  Cargo css y js propios de page templata  ----------*/
wp_enqueue_style( 'landing', get_template_directory_uri().'/template-parts/landing/css/landing.css', '', '', 'screen');

wp_enqueue_style( 'custom_media_queries', get_template_directory_uri().'/template-parts/landing/css/custom_media_queries.css', '', '', 'screen');
get_footer();
?>

<script src="<?php echo get_template_directory_uri().'/template-parts/landing/js/landing.js' ?>"></script>